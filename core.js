#!/usr/bin/node

var sys = require("sys");
var filesystem = require("fs");
var util = require("util");
var url = require("./includes/url");
var network = require("./includes/network");
var cookies = require("./includes/cookies");
var server = require("./includes/server");
var args = require("./includes/args");


argv = args.to_dict(process.argv.slice(2));

// Variaveis importantes
var server_port = "port" in argv ? argv["port"] : 8000;
var ip = "local" in argv ? "localhost" : network.getAddress();

var server_url = util.format(
    "http://%s:%s",
    ip,
    //"0.0.0.0",
    server_port
);


server.listen(server_port);
console.log("http://"+ip+":"+server_port);
