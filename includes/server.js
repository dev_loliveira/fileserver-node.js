
var fs = require("fs");
var http = require("http");
var util = require("util");
var url = require("url");
var cookies = require("./cookies");
var path = require("path");
// var jade = require("jade");


function _get_curdir(request) {
    var params = request.url.substring(request.url.indexOf("?"));
    var _cookies = cookies.getCookie(request);
    if("curdir" in _cookies) curdir = _cookies["curdir"];
    else curdir = "/home/leonardo/";
    return curdir;
}


function _get_url_without_params(request) {
    return request.url.indexOf("?") != -1 ? request.url.substring(0, request.url.indexOf("?")) : request.url;
}

function _get_url_params(request) {
    var param_str = request.url.substring(request.url.indexOf("?"));
    var result = {};

    /* Chamando a view */
    param_str = param_str.replace("?", "&");
    param_str = param_str.split("&");
    for(i in param_str) {
        if(param_str[i].length == 0) continue;
        var key = param_str[i].split("=")[0];
        var value = param_str[i].split("=")[1];
        result[key] = value;
    }

    return result;

}


function _get_link_html(link_text, rel) {
    var attr = {
        "href": "javascript:void(0);",
        "onClick": "linkClickHandler(this);",
        "rel": rel
    }
    var tag_attr = "";

    for(key in attr) {
        tag_attr += util.format("%s='%s' ", key, attr[key]);
    }

    return util.format("<a %s>%s</a>", tag_attr, link_text);
}


function direct_to_view(request, response, context) {
    var url_noparams = _get_url_without_params(request)
    var view = url_noparams.replace(/\//g, '_');
    view = view.substring(1, view.length);

    if(view[view.length-1] == "_") {
        view = view.substring(0, view.lastIndexOf("_"));
    }

    var param_dict = _get_url_params(request);
    for(key in param_dict) {
        context[key] = param_dict[key];
    }

    var curdir = _get_curdir(request);
    context["curdir"] = curdir;

    if(view in global) {
        global[ view ](request, response, context);
    } else {
        console.log(util.format("View '%s' nao existe!", view));
    }
}


exports.listen = function(port) {
    var server = http.createServer(function(request, response) {
        var curdir = _get_curdir(request);
        var url = _get_url_without_params(request);
        var serve_static = url.indexOf("/static/") != -1 ? true : false;
        var template_contents = "";
        var context = {
            "curdir": curdir,
            "show_hidden": false
        };
        var params = _get_url_params(request);

        if(serve_static) {
            var static_path = "static/" + request.url.split("/static/")[1];

            fs.readFile(static_path, function(error, contents) {
                if(error) throw error;

                response.write(contents);
                response.end();
            });
        }

        else if("curdir" in params) {
            response.writeHead(301, {
                "Location": "/",
                "Set-Cookie": "curdir="+params["curdir"]
            });
            response.end();
        }

        else if(url == "/") {
            fs.readFile("templates/dir.html", function(error, template_contents) {
                response.write(template_contents);
                response.end();
            });

        }

        else {
            direct_to_view(request, response, context);
        }



        // fs.readdir(curdir, function(error, elements) {
        //     if (error) {
        //         response.write(error);
        //     }
        //     else {
        //        // response.writeHead(200, {
        //        //    'Contents-Type': 'text/html',
        //        //    'Set-Cookie': util.format('curdir=%s', curdir)
        //        // });

        //         });
        //     }

        // });


    });

    server.listen(port);
}


/* VIEWS */
ajax_elements = function(request, response, context) {

    var curdir = context["curdir"];
    var show_hidden = "show_hidden" in context && context["show_hidden"] ? true : false;

    fs.readdir(curdir, function(error, elements) {

        /* Ordena os elementos de modo que os diretorios aparecam primeiro */
        var ordered_elements = [];
        for(i in elements) {
            var abspath = path.join(curdir, elements[i]);
            var isdir = fs.lstatSync(abspath).isDirectory();
            if(isdir)
                if(elements[i][0] != "." || (elements[i][0] == "." && show_hidden))
                    ordered_elements.push(elements[i]);
        }
        for(i in elements) {
            var abspath = path.join(curdir, elements[i]);
            var isdir = fs.lstatSync(abspath).isDirectory();
            if(!isdir)
                if(elements[i][0] != "." || (elements[i][0] == "." && show_hidden))
                    ordered_elements.push(elements[i]);
        }

        elements = ordered_elements;

        var panel = "<div class='panel panel-primary'>";
        var panel_title = "<div class='panel-heading'><h3 class='panel-title'>CURDIR</h3></div>".replace("CURDIR", curdir);
        var panel_body = "<div class='panel-body'>";

        var group_item = "<div class='list-group'>";

        if(curdir != "/") {
            var link = _get_link_html("..", "dir");
            var span = "<span class='glyphicon glyphicon-level-up'></span>&nbsp&nbsp%s";
            var item = "<div class='list-group-item'>" + util.format(span, link) + "</div>";
            group_item += item;
        }

        for(i in elements) {
            var abspath = path.join(curdir, elements[i]);
            var isdir = fs.lstatSync(abspath).isDirectory();
            var rel = isdir == true ? "dir" : "file";
            var link = _get_link_html(elements[i], rel);

            if(rel == "dir")
                var span = "<span class='glyphicon glyphicon-folder-open' aria-hidden='true'></span>&nbsp&nbsp%s";
            else if(rel == "file")
                var span = "<span class='glyphicon glyphicon-save-file'></span>&nbsp&nbsp%s";

            var item = "<div class='list-group-item'>" + util.format(span, link) + "</div>";
            group_item += item;
        }

        group_item += "</div>";
        panel_body += group_item;

        panel_body += "</div>";
        panel += panel_title + panel_body + "</div>";
        response.write(panel);
        response.end();
    });

}


ajax_cd = function(request, response, context) {
    var curdir = context["curdir"];
    var abspath = path.join(curdir, context["element"]);
    response.writeHead(301, {
        "Location": "/?curdir="+abspath
    });
    response.end();
}


ajax_downloadfile = function(request, response, context) {
    var filename = context["element"];
    var abspath = context["curdir"] + filename;
    var contents = fs.readFileSync(abspath);

    response.writeHead(200, {
        "application": "octet-stream",
        "Content-Transfer-Encoding": "binary",
        "Content-Length": contents.length,
        "Content-Disposition": util.format("attachment; filename=%s", filename)
    });
    response.write(contents);
    response.end();
}


ajax_filecontents = function(request, response, context) {
    var filename = context["fname"];
    var abspath = path.join(context["curdir"], filename);
    var contents = fs.readFileSync(abspath);

    response.writeHead(200, {"Content-type": "text"});
    response.write(contents);
    response.end();

}
