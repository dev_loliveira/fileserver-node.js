
exports.getAddress = function() {
    var os = require("os");
    var interfaces = os.networkInterfaces();
    var ip = -1;
    for (var index in interfaces) {
        var address = interfaces[index]["address"];
        if (ip == -1) ip = interfaces[index][0]["address"];
        else {
            if (interfaces[index][0]["internal"] == false)
                ip = interfaces[index][0]["address"];
        }
    }
    return (ip)
}
