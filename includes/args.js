

exports.to_dict = function(custom_args) {

    var result = {};

    for(var i=0; i<custom_args.length; i++) {
        var arg = custom_args[i];

        /* Flags que possuem valor. Podem ser passadas da seguinte forma
         * key=value
         * key value
         */
        if(arg.indexOf("--") != -1) {
            arg = arg.replace("--", "");
            if(arg.indexOf("=") == -1)
                result[arg] = custom_args[++i];
            else result[arg.split("=")[0]] = arg.split("=")[1];
        }

        else if (arg.indexOf("-") != -1) {
            result[arg.replace("-", "")] = undefined;
        }

        else {
            result[arg] = undefined;
        }
    }

    return result;
}
