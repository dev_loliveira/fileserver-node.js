
exports._get = function (request, param) {
   var url = require('url');
   var parts = url.parse(request.url, true);
   return (parts.query[param]);
}
